# Container-Image-Pipeline

A pipeline to build docker images using Kaniko to push to Gitlab Container Registry - Removing the need for Docker in Docker

## Getting started

We have provided a Docker file for an exmple of different container Images; each container image can edited and referenced within the .gitab-ci.yaml.

Gitlab CI provides a registry for each repo. which can be referenced using the following predefined Variable:

```
IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}
```

### Node Example

```
  variables:
    tag: "Node-v12"
```
The tag variable will be suffixed to the main image name

e.g. registry.gitlab.com/uaccount-repos/morsesclub-repos/morses-connect-app-build-pipeline:Node-v12

This allows multiple container images to sit in the single repo.

## kaniko build

https://github.com/GoogleContainerTools/kaniko

To allow us to build a container image within a Gitlab CI environment without Docker in Docker we use Kaniko, this can run in a container image a builds another container image through layers.

### auth with Gitlab Container Registry
We must first append a auths Json Object to the config.json to allow us to communicate with Gitlab.

```
  script:
      - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Docker/Dockerfile.$tag --destination $IMAGE_DESTINATION:$tag
```
